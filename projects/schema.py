import graphene

from graphene_django.types import DjangoObjectType

from .models import Project, Highlight

class ProjectType(DjangoObjectType):
    class Meta:
        model = Project


class HighlightType(DjangoObjectType):
    class Meta:
        model = Highlight

class Query(object):
    all_projects = graphene.List(ProjectType)
    all_highlights = graphene.List(HighlightType)

    def resolve_all_projects(self, info, **kwargs):
        return Project.objects.all()

    def resolve_all_highlights(self, info, **kwargs):
        # We can easily optimize query count in the resolve method
        return Highlight.objects.select_related('project').all()