from django.db import models


class Highlight(models.Model):
  text = models.CharField(max_length=20)

  def __str__(self):
    return self.text

class Project(models.Model):
  title = models.CharField(max_length=30)
  desc = models.TextField()
  photo = models.TextField()
  link = models.TextField()
  highlights = models.ManyToManyField(Highlight)

  def __str__(self):
    return self.title
