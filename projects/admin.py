from django.contrib import admin
from .models import Project,Highlight

admin.site.register(Project)
admin.site.register(Highlight)

